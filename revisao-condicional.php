<?php

$nome = "Laís Osaka";
$idade = 33;
$email = "laismidori51@gmail.com";
$senha = "12345678";
$cursos = ["PHP", "HTML", "CSS"];


echo "<h1>Trabalhando com Estrutura Condicional</h1>";

echo "<h2>Exemplo de if (se...)</h2>";

if ($idade >= 18) {
    echo "O usuario $nome é maior de idade";
}

echo "<hr>";

echo "<h2> Exemplo de if Ternário</h2>";
echo ($idade >= 18) ? "Maior de Idade" : "Menor de Idade";

echo "<hr>";

echo "<h2> Exemplo de if e else </h2>";

if ($email == "laismidori51@gmail.com" && $senha == "12345678") {
    echo "Usuário Logado";
} else {
    echo "Usuario ou senha Invalido!";

    //colocar no echo sempre desta forma pq se um racker tentar invadir será mais dificil
}

echo "<h2> Exemplo Login de if e else </h2>";

if ($email == "laismidori51@gmail.com") {
    if ($senha == "12345678") {
        echo "Usuário Logado";
    } else {
        echo "Usuario ou senha Invalido!";
    }
} else {
    echo "Usuario ou senha Invalido!";
}

echo "<hr>";

echo "<h2> Exemplo de multiplas Condições </h2>";

$num1 = 10;
$num2 = 20;

if ($num1 == $num2) {
    echo "Os numeros são iguais";
} elseif ($num1 > $num2) {
    echo "O número 1 é maior que o número 2";
} else {
    echo "O número 2 é maior que o número 1";
}

echo "<hr>";

echo "<h2> Exemplo de GET</h2>";

//verificar se o menu esta vazio atraves do ??, ai o erro para
$menu = $_GET["menu"] ?? "Home";

//verifica a variavel apenas
//strtolower ele vai converter a escrita em maiusculo ou minusculo

switch (strtolower($menu)) {
    case "Home":
        echo "Página Principal";
        break;

    case "Empresa":
        echo "Página Empresa";
        break;

    case "Produtos":
        echo "Página Produtos";
        break;

    case "Contato":
        echo "Página Contato";
        break;

    default:
        echo "Página Erro 404";
}
