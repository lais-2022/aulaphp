<?php
$nome = "Laís Midori";
$cargo = "Tecnico";
$cpf = "354.002.6798-1";
$rg = "12.002.123-0";
$cnpj = "10.100.211/0001-02";
$cep = "17.525-000"

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Declaração de local de trabalho</title>
</head>
<body>
    
<h1>DECLARAÇÃO DE LOCAL DE TRABALHO</h1>
<br>
<br>
<br>
<p>Eu, <?=$nome ?>, brasileiro, no cargo <?=$cargo ?>, inscrito(a)no CPF nº<?=$cpf ?> e no RG nº<?=$rg ?>, declaro para os devidos fins que possuo vínculo empregatício com a empresa Senac inscrita no CNPJ sob o nº <?=$cnpj ?> , localizada à R.Paraíba,125 ‐ Marília,SP, CEP Nº <?=$cep ?>.
</p>

<p>Por ser expressão da verdade, firmo a presente para efeitos legais.</p>
  
<p>Marília–SP,15 de Setembrode 2022</p>
<br>
<br>
<br>
<p><?=$nome ?></p>

</body>
</html>