<?php

function dividir($num1, $num2)
{
    $total = $num1 / $num2;
    return $total;
}

function subtracao($num1, $num2)
{
    $total = $num1  - $num2;
    return $total;
}

function multiplicacao($num1, $num2)
{
    $total = $num1 * $num2;
    return $total;
}
function soma($num1, $num2)
{
    $total = $num1 + $num2;
    return $total;
}

echo soma(10, 5);
echo "<hr>";
echo dividir(10, 2);
echo "<hr>";
echo subtracao(10, 5);
echo "<hr>";
echo multiplicacao(10, 2);

echo "<br>";

function par_ou_impar($numero){

if ($numero %2 == 0) {
    return "numero é par";
    }

    return "número é impar";
}

echo par_ou_impar(10);

//outra forma mais resumida para codigo

function par_ou_impar_mod2($numero){
    return ($numero % 2 == 0) ? "numero é par": "Número é impar";
}

echo "<hr>";

function geradoSenhaComFor($senhaInicial = 1, $senhaFinal = 20){
    for($contador = $senhaInicial; $contador <= $senhaFinal; $contador++ ){
        echo $contador. " - ";
    }
}

geradoSenhaComFor(10, 20);

echo "<hr>";

function geradoSenhaComWhile($senhaInicial = 1, $senhaFinal = 10){
    $contador = $senhaInicial;
  
while ($contador <= $senhaFinal) {
    echo $contador. " - ";
    $contador++;
    }   
}

geradoSenhaComWhile(10, 20);