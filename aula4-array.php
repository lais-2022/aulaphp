<?php

#array unidimencional
$listaCompra = ["arroz", "feijão", "banana", "detergente", "sabonete"];

#para adicionar outro item no array $listaCompra[]="...."


print_r($listaCompra);

echo "<hr>";

var_dump($listaCompra);

echo "<hr>";

echo $listaCompra[2];

echo "<hr>";
echo $listaCompra[4].", ".$listaCompra[2];

echo "<hr>";

/*
foreach ($variable as $key => $value) {

}
*/

foreach ($listaCompra as $item) {
    echo $item;
    echo "<br>";
}

echo "<br>";

#array associativo

$funcionario = [
    "nome" => "Laís Midori",
    "cargo" => "Estudante",
    "idade" => 33,
    "salario" => 500.50,
    "ativo" => true
];

var_dump($funcionario);

echo $funcionario["cargo"];

echo "<hr>";

#array multidimensional

$funcionarios = [
   [
    "nome" => "Laís Midori",
    "cargo" => "Estudante",
    "idade" => 33,
    "salario" => 500.50,
    "ativo" => true,
    "cursos" => ["Web", "PHP", "JavaScript"]
   ],
   [
    "nome" => "João Luis",
    "cargo" => "Estudante",
    "idade" => 30,
    "salario" => 1000.50,
    "ativo" => false,
    "cursos" => []
   ],
   [
    "nome" => "Maria Du",
    "cargo" => "Estudante",
    "idade" => 40,
    "salario" => 800.50,
    "ativo" => true,
    "cursos" => ["Photoshop", "Ilustrator"]
   ]
];

var_dump($funcionarios);

echo "<hr>";

echo "nome:";
echo $funcionarios[2]["nome"];

echo "<br>";

echo "cargo:";
echo $funcionarios[2]["cargo"];

echo "<br>";

echo "cursos:";
echo $funcionarios[2]["cursos"][1];

#echo "cursos:". $funcionarios[2]["cursos"][1];

echo "<hr>";

foreach ($funcionarios as $item) {
    echo "nome:".$item["nome"];
    echo "<br>";

    echo "cargo:".$item["cargo"];
    echo "<br>";

    echo "idade:".$item["idade"];
    echo "<br>";

    echo "cursos:".$item["cursos"];
    echo "<hr>";
}
